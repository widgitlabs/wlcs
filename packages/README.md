# Managing Packages

This repository uses [lerna] to manage Widgit Labs modules and publish them as packages to [npm]. This enforces certain steps in the workflow which are described below.

## Managing Dependencies

There are two types of dependencies that you might want to add to one of the existing Widgit Labs packages.

### Production Dependencies

Production dependencies are stored in the `dependencies` section of the package’s `package.json` file.

#### Adding New Dependencies

The simplest way to add a production dependency to one of the packages is to run a very convenient [lerna add](https://github.com/lerna/lerna/tree/master/commands/add#readme) command from the root of the project.

_Example:_

```shell
lerna add stylelint-csstree-validator packages/stylelint-config
```

This command adds the latest version of `stylelint-csstree-validator` as a dependency to the `@widgitlabs/stylelint-config` package, which is located in `packages/stylelint-config` folder.

#### Removing Existing Dependencies

Removing a dependency from one of the Widgit Labs packages requires some manual work. You need to remove the line in the corresponding `dependencies` section of the `package.json` file.

_Example:_
```diff
+++ b/packages/eslint-config-widgitlabs/package.json
@@ -33,7 +33,6 @@
 		"chalk": "^2.4.1",
 		"eslint": "^5.10.0",
 		"eslint-config-react-app": "^3.0.5",
-		"eslint-plugin-flowtype": "^3.2.0",
 		"eslint-plugin-import": "^2.14.0",
 		"eslint-plugin-jsx-a11y": "^6.1.2",
 		"eslint-plugin-react": "^7.11.1"
```

Next, you need to run `npm install` in the root of the project to ensure that `package-lock.json` file gets properly regenerated.

#### Updating Existing Dependencies

This is the most confusing part of working with [lerna] which causes a lot of hassles for contributors. The most successful strategy so far is to do the following:
 1. First, remove the existing dependency as described in the previous section.
 2. Next, add the same dependency back as described in the first section of this chapter. This time it wil get the latest version applied unless you enforce a different version explicitly.

### Development Dependencies

In contrast to production dependencies, development dependencies shouldn't be stored in individual Widgit Labs packages. Instead they should be installed in the project's `package.json` file using the usual `npm install` command. In effect, all development tools are configured to work with every package at the same time to ensure they share the same characteristics and integrate correctly with each other.

_Example:_

```shell
npm install glob --save-dev
```

This commands adds the latest version of `glob` as a development dependency to the `package.json` file. It has to be executed from the root of the project.

## Maintaining Changelogs

In maintaining npm packages it can be tough to keep track of changes. To simplify the release process the project root contains a `CHANGELOG.md` file which details all published releases and the unreleased changes, if any exist.

For each pull request, you should always include relevant changes under the "Unreleased" heading at the top of the file. You should add the heading if it doesn't already exist.

_Example:_

```md
## [Unreleased]

### Added:
 - Added stylelint `stylelint-csstree-validator` plugin to `@widgitlabs/stylelint-config`

### Removed
 - Removed ESLint `eslint-plugin-flowtype` config from `eslint-plugin-widgitlabs`
```

Our changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and we adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). It is important that you describe your changes accurately, since this is used in the packages release process to help determine the version of the next release. Changelog entries should be one of the following types:

- `Added` for new features.
- `Changed` for changes to existing functionality.
- `Deprecated` for soon-to-be-removed features.
- `Removed` for now removed features.
- `Fixed` for bug fixes.
- `Security` in case of vulnerabilities.

Remember:

- Changelogs are _for humans_, not machines.
- There should be an entry for every version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version should be displayed.

## Releasing Packages

Lerna automatically releases all outdated packages. To check which packages are outdated and will be released, type `npm run publish:check`.

If you have the ability to publish packages, you _must_ have [2FA enabled](https://docs.npmjs.com/getting-started/using-two-factor-authentication) on your [npm account][npm].

### Before Releasing

Confirm that you're logged in to [npm], by running `npm whoami`. If you're not logged in, run `npm adduser` to login.

If you're publishing a new package, ensure that its `package.json` file contains the correct `publishConfig` settings:

```json
{
	"publishConfig": {
		"access": "public"
	}
}
```

You can check your package configs by running `npm run lint-pkg-json`.

### Development Release

Run the following command to release a dev version of the outdated packages.

```shell
npm run publish:dev
```

Lerna will ask you which version number you want to choose for each package. For a `dev` release, you'll more likely want to choose the "prerelease" option. Repeat the same for all the outdated packages and confirm your version updates.

Lerna will then publish to [npm], commit the `package.json` changes and create the git tags.

### Production Release

To release a production version for the outdated packages, run the following command:

```shell
npm run publish:prod
```

Choose the correct version based on `CHANGELOG.md` files, confirm your choices and let Lerna do its magic.

## Credits

Thanks to the HumanMade, WordPress and Gutenberg contributors to which this [document is based on](https://github.com/humanmade/coding-standards/blob/master/packages/README.md).

[lerna]: https://lerna.js.org/
[npm]: https://www.npmjs.com/
