# @widgitlabs/eslint-config

Widgit Labs coding standards for JavaScript.

## Installation

This package is an ESLint shareable configuration, and requires: `babel-eslint`, `eslint`, `eslint-config-react-app`, `eslint-plugin-flowtype`, `eslint-plugin-import`, `eslint-plugin-jsx-a11y`, `eslint-plugin-jsdoc`, `eslint-plugin-react`, `eslint-plugin-react-hooks`, `eslint-plugin-sort-destructure-keys`.

To install this config and the peerDependencies when using **npm 5+**:

```
npx install-peerdeps --dev @widgitlabs/eslint-config@latest
```

You can then use it directly on the command line:

```shell
./node_modules/.bin/eslint -c @widgitlabs/eslint-config MyFile.js
```

Alternatively, you can create your own configuration and extend these rules:
```yaml
extends:
- @widgitlabs/eslint-config
```

### Working with TypeScript

If you desire to use TypeScript for your project, you will need to add another dependency:

```shell
npm install --save-dev @typescript-eslint/parser
```

Once it's installed, update your configuration with the `parser` parameter:

```yml
parser: "@typescript-eslint/parser"
extends:
    - @widgitlabs/eslint-config
```

## Global Installation

When installing globally, you need to ensure the peer dependencies are also installed globally.

Run the same command as above, but instead with `--global`:

```shell
npx install-peerdeps --global @widgitlabs/eslint-config@latest
```

This allows you to use `eslint -c widgitlabs MyFile.js` anywhere on your filesystem.
